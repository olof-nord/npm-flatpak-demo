FLATPAK=$(shell which flatpak)
FLATPAK_BUILDER=$(shell which flatpak-builder)
FLATPAK_NODE_GENERATOR=$(shell which flatpak-node-generator)

FLATPAK_MANIFEST=com.gitlab.npm-flatpak-demo.yml
FLATPAK_APPID=com.gitlab.npm-flatpak-demo
FLATPAK_BUILD_FLAGS=--verbose --force-clean
FLATPAK_INSTALL_FLAGS=--verbose --user --install --force-clean
FLATPAK_DEBUG_FLAGS=--verbose --run

.PHONY: build-dependencies
build-dependencies:
	$(FLATPAK) install org.freedesktop.Platform//21.08
	$(FLATPAK) install org.freedesktop.Sdk//21.08
	$(FLATPAK) install org.freedesktop.Sdk.Extension.node16//21.08

.PHONY: generate-dependencies
generate-dependencies:
	$(FLATPAK_NODE_GENERATOR) npm package-lock.json \
		--recursive \
		--output generated-sources.json

.PHONY: build
build:
	$(FLATPAK_BUILDER) build $(FLATPAK_BUILD_FLAGS) $(FLATPAK_MANIFEST)

.PHONY: debug
debug:
	$(FLATPAK_BUILDER) $(FLATPAK_DEBUG_FLAGS) build $(FLATPAK_MANIFEST) sh

.PHONY: install
install:
	$(FLATPAK_BUILDER) build $(FLATPAK_INSTALL_FLAGS) $(FLATPAK_MANIFEST)

.PHONY: run
run:
	$(FLATPAK) run $(FLATPAK_APPID)

.PHONY: update-submodules
update-submodules:
	git submodule foreach git pull

.PHONY: install-flatpak-node-generator
install-flatpak-node-generator:
	pipx install ./flatpak-builder-tools/node