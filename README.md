# npm-flatpak-demo

Demo NPM/Node app with Flatpak packaging, following the requirements of Flathub publishing.

## Update dependencies

A Flathub release must specify all its build dependencies, and may not use the `--share=network` flag.

Instead, a fixed list of dependencies is used. To generate these, Flathub provides some python scripts.

## NPM

For NPM, [flatpak-node-generator](https://github.com/flatpak/flatpak-builder-tools/blob/master/node/README.md) is
provided.

Using this as listed below gives us a list of dependencies.

```shell
python3 flatpak-builder-tools/node/flatpak-node-generator.py \
    npm package-lock.json \
    --recursive \
    --xdg-layout \
    --output generated-sources.json
```

## Build, Install and Run

To build, use `make build`.

To install, use `make install`.

To run, use `make run`.